var app = angular.module("veebiprojekt", ['ngRoute', 'ui.bootstrap','bootstrapLightbox', 'angular-carousel']);

var teams = [
    {
        name: "Fnatic",
        shortname: "fnatic",
        players: ["flusha", "KRIMZ", "olofmeister", "JW", "dennis"],
        points: 958
    },
    {
        name: "Luminosity Gaming",
        shortname: "lg",
        players: ["FalleN", "fer", "TACO", "coldzera", "fnx"],
        points: 876
    },
    {
        name: "Natus Vincere",
        shortname: "navi",
        players: ["Zeus", "Edward", "GuardiaN", "seized", "flamie"],
        points: 849
    },
    {
        name: "Astralis",
        shortname: "astralis",
        players: ["karrigan", "dupreeh", "dev1ce", "Xyp9x", "cajunb"],
        points: 732
    },
    {
        name: "Virtus.Pro",
        shortname: "virtuspro",
        players: ["pasha", "byali", "Snax", "NEO", "TaZ"],
        points: 307
    },
    {
        name: "EnVyUs",
        shortname: "envyus",
        players: ["Happy", "NBK", "kennyS", "apEX", "DEVIL"],
        points: 278
    },
    {
        name: "Ninjas in Pyjamas",
        shortname: "nip",
        players: ["f0rest", "friberg", "GeT_RiGhT", "Xizt", "pyth"],
        points: 245
    },
    {
        name: "Liquid",
        shortname: "liquid",
        players: ["nitr0", "EliGE", "s1mple", "Hiko", "koosta"],
        points: 215
    },
    {
        name: "Dignitas",
        shortname: "dignitas",
        players: ["tenzki", "MSL", "Kjaerbye", "k0nfig", "RUBINO"],
        points: 201
    },
    {
        name: "Counter Logic Gaming",
        shortname: "clg",
        players: ["reltuC", "hazed", "tarik", "jdm64", "FugLy"],
        points: 180
    }
];

var tournaments = [
    {
        name: "MLG Columbus 2016",
        prize: "1000000",
        imageurl: "images/tournaments/mlg2016.jpg"
    },
    {
        name: "ESL Katowice 2015",
        prize: "250000",
        imageurl: "images/tournaments/katowice2015.jpg"
    },
    {
        name: "PGL Cluj Napoca 2015",
        prize: "250000",
        imageurl: "images/tournaments/cluj2015.jpg"
    },
    {
        name: "ESL Cologne 2015",
        prize: "250000",
        imageurl: "images/tournaments/cologne2015.jpg"
    }
];

app.config(["$routeProvider", "$locationProvider", function($routeProvider, $locationProvider){
    $routeProvider
        .when("/", {
            templateUrl: "pages/home.html",
            controller : "HomeController"
        })
        .when("/teams", {
            templateUrl: "pages/teams.html",
            controller : "TeamsController"
        })
        .when("/tournaments", {
            templateUrl: "pages/tournaments.html",
            controller: "TournamentsController"
        });
}]);

app.controller("HomeController", ['$scope', function($scope){
    // nothing to be done here
}]);

app.controller("HeaderController", ['$scope', '$location', function($scope, $location){
    $scope.isActive = function(viewLocation){
        return viewLocation === $location.path();
    }
}]);

app.controller("TeamsController", ['$scope', 'Lightbox', function($scope, Lightbox){
    $scope.teams = teams;
    
    $scope.openLightboxModal = function(teamName, index){
        var array = [];
        console.log(index);
        for(var i=0;i<teams.length;i++){
            array.push(
                {
                    url: "images/teams/"+teams[i].shortname+".png",
                    caption: teams[i].name,
                    thumbUrl: "images/teams/thumbs/"+teams[i].shortname+".png"
                }
            );
        }
        Lightbox.openModal(array, index);
    };
    
    $scope.openPlayerLightboxModal = function(teamName, index){
        var array = [];   
        var players = getTeamPlayers(teamName);
        console.log(index);
        for(var i=0;i<players.length;i++){
            array.push(
                {
                    url: "images/players/"+players[i].toLowerCase()+".jpg",
                    caption: players[i],
                    thumbUrl: "images/players/thumbs/"+players[i].toLowerCase()+".jpg"
                }
            );
        }
        Lightbox.openModal(array, index);
    };
}]);

app.controller("TournamentsController", ['$scope', function($scope){
    $scope.tournaments = tournaments;
}]);

function getTeamPlayers(teamName){
    for(var i=0;i<teams.length;i++){
        if(teams[i].name === teamName){
            return teams[i].players;
        }
    }
}
